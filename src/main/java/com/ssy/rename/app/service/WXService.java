package com.ssy.rename.app.service;

import com.alibaba.fastjson.JSONObject;
import com.ssy.rename.app.constant.WeiXinConstant;
import com.ssy.rename.commons.util.WeiXinClientUtil;
import com.ssy.rename.commons.util.WeiXinResponseWrapper;
import org.apache.commons.httpclient.NameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


import java.io.IOException;

@Service
public class WXService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public String getAccessToken() {
        NameValuePair[] params = new NameValuePair[]{
                new NameValuePair("grant_type", "client_credential"),
                new NameValuePair("appid", WeiXinConstant.APP_ID),
                new NameValuePair("secret", WeiXinConstant.APP_SECRET)
        };
        WeiXinResponseWrapper response = null;
        try {
            response = WeiXinClientUtil.sendGetRequest(WeiXinConstant.ACCESS_TOKEN, params);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        if (!response.isRequestSucceed()) {
            log.error(response.getErrCode()+response.getErrMsg());
        }
        return response.getData().getString("access_token");
    }

    public WeiXinResponseWrapper checkText(String text){
        NameValuePair[] params = new NameValuePair[]{
                new NameValuePair("access_token", getAccessToken()),
        };
        WeiXinResponseWrapper response = null;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("content",text);
        String body = jsonObject.toJSONString();
        try {
            response = WeiXinClientUtil.sendPostRequest(WeiXinConstant.CHECK_TEXT, params,body);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        if (!response.isRequestSucceed()) {
            log.error(response.getErrCode()+response.getErrMsg());
        }
        return response;
    }

}
