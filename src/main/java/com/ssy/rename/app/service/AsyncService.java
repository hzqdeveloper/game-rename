package com.ssy.rename.app.service;

import com.ssy.rename.app.mapper.ReNameHistoryMapper;
import com.ssy.rename.commons.entity.ReNameHistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncService {

    @Autowired
    private ReNameHistoryMapper reNameHistoryMapper;

    /**
     * 异步记录用户生成的名字
     * @param reNameHistory
     */
    @Async
    public void createReNameHistory(ReNameHistory reNameHistory){

        reNameHistoryMapper.createReNameHistory(reNameHistory);
    }

    @Async
    public void sendEmailMessage(){

    }
}
