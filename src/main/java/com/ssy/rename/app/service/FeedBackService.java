package com.ssy.rename.app.service;

import com.ssy.rename.app.mapper.FeedBackMapper;
import com.ssy.rename.commons.entity.FeedBack;
import com.ssy.rename.web.vm.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FeedBackService {

    @Autowired
    private FeedBackMapper feedBackMapper;
    
    
    public ResultData createFeedBack(FeedBack feedBack){
        return feedBackMapper.createFeedBack(feedBack)>0?ResultData.ok():ResultData.error(10001,"反馈失败");
    }
}
