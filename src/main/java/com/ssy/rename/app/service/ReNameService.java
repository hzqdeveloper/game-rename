package com.ssy.rename.app.service;

import com.alibaba.fastjson.JSONObject;
import com.ssy.rename.commons.entity.ReNameHistory;
import com.ssy.rename.commons.util.ByteUtil;
import com.ssy.rename.commons.util.RandomUtils;
import com.ssy.rename.commons.util.RedisUtil;
import com.ssy.rename.commons.util.UnicodeUtil;
import com.ssy.rename.web.vm.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.ssy.rename.app.config.CacheNames.HOT_USER_GAME_NAME_RANKING_LIST;

@Service
public class ReNameService {

    @Autowired
    private RandomUtils randomUtils;

    @Autowired
    private UnicodeUtil unicodeUtil;

    @Autowired
    private ByteUtil byteUtil;


    @Autowired
    private AsyncService asyncService;


    @Autowired
    private RedisUtil redisUtil;

    public ResultData getRankingList(String name) {
        if (name == null) {
            return ResultData.requiredParamIsNull();
        }
        String key = HOT_USER_GAME_NAME_RANKING_LIST + name;
        if (redisUtil.hasKey(key)) {
            return ResultData.ok(redisUtil.get(key));
        }
        return ResultData.error(10046, "当前游戏暂时没有排行");
    }

    public ResultData<String> getReName(String name, String gameCd) {

        //异步记录 用户使用过的名字
        asyncService.createReNameHistory(new ReNameHistory(name, gameCd));

        StringBuilder rename = new StringBuilder();

        int bytes = byteUtil.getBytes(name);//按照腾讯游戏名字长度规范 英文英文字符=1byte 中文 中文字符=2byte 总长度小于maxLen(byte)

        int maxLen = 0;
        switch (gameCd) {
            case "wzry":
                maxLen = 10;
                break;
            case "cfm":
                maxLen = 12;
                break;
            case "lolm":
                maxLen = 12;
            case "hpjy":
                maxLen = 12;
                break;
            default:
                return ResultData.requiredParamIsNull();
        }
        for (int i = 0; i < name.length(); i++) {//循环拼接
            if (bytes <= maxLen) {//如果bytes小于等于maxLen执行拼接 大于maxLen名字超长 1unicode在腾讯游戏=2byte
                if (i + 1 == name.length()) {//如果走到最后一个字符的拼接了  还有多余位置的话 继续拼接unicode
                    rename.append(name.charAt(i));
//                    bytes += 2;
                    while (true) {//循环拼接到字数够 防止热名生成名字重复高概率
                        if (bytes > maxLen) {
                            break;
                        }
                        bytes += 2;
                        rename.append(unicodeUtil.unicode2String(unicodeUtil.getInVisibleUnicode(randomUtils.getRandom())));
                    }
                    continue;
                }
                if (randomUtils.useUnicode() && bytes <= maxLen) {//概率进unicode拼接 不同位置拼接 不容易名字冲突
                    rename.append(name.charAt(i) + unicodeUtil.unicode2String(unicodeUtil.getInVisibleUnicode(randomUtils.getRandom())));
                    bytes += 2;
                    continue;
                }
            }
            rename.append(name.charAt(i));
        }

        return ResultData.ok(rename.toString());
    }
}
