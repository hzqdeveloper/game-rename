package com.ssy.rename.app.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ssy.rename.app.constant.WZRYConstant;
import com.ssy.rename.commons.entity.wzry.*;
import com.ssy.rename.commons.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static com.ssy.rename.app.config.CacheNames.*;

@Service
public class HonorService {

    @Qualifier(value = "agvRestTemplate")
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private RedisUtil redisUtil;

    private HttpEntity<Map<String, Object>> generatePostJson(Map<String, Object> jsonMap, HttpHeaders httpHeaders) {

        MediaType type = MediaType.parseMediaType("application/json;charset=UTF-8");

        httpHeaders.setContentType(type);

        HttpEntity<Map<String, Object>> httpEntity = new HttpEntity<>(jsonMap, httpHeaders);

        return httpEntity;
    }

    private List<RankListBean> getRankList(String adCode, Integer heroId, Integer areaId) {
        Map<String, Object> params = new HashMap<>();
        params.put("adcode", Integer.parseInt(adCode));
        params.put("heroId", heroId);
        params.put("areaId", areaId);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("token", "qasmnMRr");
        httpHeaders.add("userId", "367206187");
        String uri = WZRYConstant.HONOR_RANK_LIST_URL;
        ResponseEntity<WZRYResultBean> rankListResult = restTemplate.postForEntity(uri, generatePostJson(params, httpHeaders), WZRYResultBean.class);
        if (rankListResult != null) {
            WZRYResultBean body = rankListResult.getBody();
            if (body.getReturnCode() == 0) {
                String key = WZRY_HONOR_RANK_LIST + "areaId:" + areaId + "+adCode:" + adCode + "heroId:" + heroId;
                List<RankListBean> rankListBeans = (List<RankListBean>) body.getData().getList();
                redisUtil.set(key, rankListBeans);
                return rankListBeans;
            }
        }
        return new ArrayList<>();
    }


    /**
     * 整合，所有战区榜单
     */
    public void megerHonorList(){
        for (int i = 1; i <= 4; i++) {
             mergeHonorListByAreaId(1);
        }
    }

    private void mergeHonorListByAreaId(Integer areaId) {
        List<HeroBean> heroList = getHeroList();
        String jsonString = JSONObject.toJSONString(heroList);
        heroList = JSON.parseArray(jsonString,HeroBean.class);
        for (HeroBean item :
                heroList) {
            mergeHonorListByHeroIdAndAreaId(item.getEname(), areaId);
        }
    }

    private void mergeHonorListByHeroIdAndAreaId(Integer heroId, Integer areaId) {
        List<WZRYProvince> districtsResult = null;
        if (redisUtil.hasKey(WZRY_DISTRICTS)) {
            districtsResult = (List<WZRYProvince>) redisUtil.get(WZRY_DISTRICTS);
        } else {
            districtsResult = getDistricts();
        }
//        JSONObject jsonObject = JSONObject.parseObject(districtsResult);
        String jsonString = JSONObject.toJSONString(districtsResult);
        districtsResult = JSON.parseArray(jsonString,WZRYProvince.class);
        for (WZRYProvince provinceItem :
                districtsResult) {
            String adCode = provinceItem.getAdcode();//获取地区编号
            getRankList(adCode, heroId, areaId);
        }
    }


    public List<WZRYProvince> getDistricts() {
        Map<String, Object> params = new HashMap<>();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("token", "qasmnMRr");
        httpHeaders.add("userId", "367206187");
        String uri = WZRYConstant.DISTRICTS_URL;
        ResponseEntity<WZRYResultBean> rankListResult = restTemplate.postForEntity(uri, generatePostJson(params, httpHeaders), WZRYResultBean.class);
        if (rankListResult != null) {
            WZRYResultBean body = rankListResult.getBody();
            if (body.getReturnCode() == 0) {
                List<WZRYProvince> rankListBeans = (List<WZRYProvince>) body.getData().getList();
                redisUtil.set(WZRY_DISTRICTS, rankListBeans);
                return rankListBeans;
            }
        }
        return new ArrayList<>();
    }

    public List<HeroBean> getHeroList() {
        String uri = WZRYConstant.WZRY_HERO_LIST_URL;
        HeroBean[] heroListResult = restTemplate.getForEntity(uri, HeroBean[].class).getBody();
        if (heroListResult.length > 0) {
            List<HeroBean> heroBeans = Arrays.asList(heroListResult);
            redisUtil.set(WZRY_HERO_LIST, heroBeans);
            return heroBeans;
        }
        return null;
    }
}
