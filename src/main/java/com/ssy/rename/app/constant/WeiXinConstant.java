package com.ssy.rename.app.constant;

public class WeiXinConstant {

    public static final String LOGIN_GRAND_TYPE = "authorization_code";
    public static final String ACCESS_TOKEN_GRAND_TYPE = "client_credential";
    public static final String APP_ID = "wx99c239faf39a904e";
    public static final String APP_SECRET = "de1fce57327f5ddf82f45ab9a2701180";


    public static final String ACCESS_TOKEN_PATH = "https://api.weixin.qq.com/sns/jscode2session";

    public static final String CHECK_TEXT = "https://api.weixin.qq.com/wxa/msg_sec_check";
    public static final String ACCESS_TOKEN= "https://api.weixin.qq.com/cgi-bin/token";
    public static final String IMG_FILTER_URL= "https://api.weixin.qq.com/wxa/img_sec_check";
    public static final Long ACCESS_TOKEN_EXPIRE_TIME=7000L;



}
