package com.ssy.rename.app.constant;

public class WZRYConstant {

    public static final String HONOR_RANK_LIST_URL = "https://kohcamp.qq.com/honor/ranklist";
    public static final String DISTRICTS_URL = "https://kohcamp.qq.com/honor/districts";
    public static final String WZRY_HERO_LIST_URL ="https://pvp.qq.com/web201605/js/herolist.json";
}
