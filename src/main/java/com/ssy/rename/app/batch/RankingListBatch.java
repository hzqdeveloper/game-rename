package com.ssy.rename.app.batch;

import com.ssy.rename.app.constant.WZRYConstant;
import com.ssy.rename.app.mapper.ReNameHistoryMapper;
import com.ssy.rename.app.service.HonorService;
import com.ssy.rename.commons.dto.ReNameHotRankingListDTO;
import com.ssy.rename.commons.entity.ReNameHistory;
import com.ssy.rename.commons.entity.wzry.WZRYProvince;
import com.ssy.rename.commons.entity.wzry.WZRYResultBean;
import com.ssy.rename.commons.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ssy.rename.app.config.CacheNames.HOT_USER_GAME_NAME_RANKING_LIST;
import static com.ssy.rename.app.config.CacheNames.WZRY_DISTRICTS;

@Component
public class RankingListBatch {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ReNameHistoryMapper reNameHistoryMapper;

    @Autowired
    private HonorService honorService;

    @Autowired

    private RestTemplate restTemplate;

    @Autowired
    private RedisUtil redisUtil;

    @Scheduled(fixedDelay = 1000*60*5)
//    @Scheduled(fixedDelay = 1000)
    public void gameNameRankingList(){
        List<ReNameHotRankingListDTO> wzryUserGameNames = reNameHistoryMapper.findHotUserGameNameByGameCd("wzry");
        List<ReNameHotRankingListDTO> lolmUserGameNames = reNameHistoryMapper.findHotUserGameNameByGameCd("lolm");
        List<ReNameHotRankingListDTO> cfmUserGameNames = reNameHistoryMapper.findHotUserGameNameByGameCd("cfm");
        List<ReNameHotRankingListDTO> hpjyUserGameNames = reNameHistoryMapper.findHotUserGameNameByGameCd("hpjy");

        redisUtil.set(HOT_USER_GAME_NAME_RANKING_LIST+"wzry",wzryUserGameNames);
        redisUtil.set(HOT_USER_GAME_NAME_RANKING_LIST+"cfm",cfmUserGameNames);
        redisUtil.set(HOT_USER_GAME_NAME_RANKING_LIST+"lolm",lolmUserGameNames);
        redisUtil.set(HOT_USER_GAME_NAME_RANKING_LIST+"hpjy",hpjyUserGameNames);

    }

//    //王者荣耀地区，七天更新一次
//    @Scheduled(fixedDelay = 1000*60*60*24*7)
//    public void districts(){
//        honorService.getDistricts();
//    }
//
//    //王者荣耀角色，一天更新一次
//    @Scheduled(fixedDelay = 1000*60*60*24)
//    public void heroList(){
//        honorService.getHeroList();
//    }
//
//    @Scheduled(fixedDelay = 1000*60*60*24)
//    public void honorRankList(){
//        honorService.megerHonorList();
//    }
}
