package com.ssy.rename.app.config;

/**
 * create by huangziqideveloper@gmail.com
 */
public interface CacheNames {

//    String SYSTEM_PROPERTIES_GROUPID_RESOURCEID = "rename:system:properties:";
    String HOT_USER_GAME_NAME_RANKING_LIST = "rename:hot:user:game:name:ranking:list:";
    String WZRY_DISTRICTS = "rename:wzry:districts";
    String WZRY_HERO_LIST = "rename:wzry:hero:list";
    String WZRY_HONOR_RANK_LIST = "rename:wzry:honor:rank:list:";
}
