package com.ssy.rename.app.mapper;

import com.ssy.rename.commons.dto.ReNameHotRankingListDTO;
import com.ssy.rename.commons.entity.ReNameHistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ReNameHistoryMapper {

    List<ReNameHotRankingListDTO> findHotUserGameNameByGameCd(@Param("gameCd") String gameCd);

    List<ReNameHotRankingListDTO> findHotUserGameName();

    Integer createReNameHistory(ReNameHistory reNameHistory);

}
