package com.ssy.rename.app.mapper;

import com.ssy.rename.commons.entity.FeedBack;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FeedBackMapper {

    int createFeedBack(FeedBack feedBack);

}
