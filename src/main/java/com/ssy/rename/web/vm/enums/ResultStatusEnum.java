package com.ssy.rename.web.vm.enums;

/**
 * create by huangziqideveloper@gmail.com
 */
public enum ResultStatusEnum {
    CONFIG_NOTFOUND(10001,"当前config不存在"),
    USER_EXISTS(10002,"账号不存在"),
    PWD_INVALID(10003,"密码错误"),
    ;

    private Integer code;

    private String message;

    ResultStatusEnum(Integer code,String message){
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
