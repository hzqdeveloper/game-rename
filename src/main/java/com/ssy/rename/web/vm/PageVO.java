package com.ssy.rename.web.vm;


import java.io.Serializable;

/**
 * create by huangziqideveloper@gmail.com
 */
public class PageVO implements Serializable {
	private Integer firstResult;//从哪条开始

	private Integer current = 1;//第几页

	private Integer size = 15;// 一页多少数据

	public Integer getFirstResult() {
		return firstResult;
	}

	public void setFirstResult(Integer firstResult) {
		this.firstResult = firstResult;
	}

	public Integer getCurrent() {
		return current;
	}

	public void setCurrent(Integer current) {
		this.current = current;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}
}
