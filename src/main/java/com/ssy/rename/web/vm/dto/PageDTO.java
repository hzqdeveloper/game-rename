package com.ssy.rename.web.vm.dto;

/**
 * create by huangziqideveloper@gmail.com
 */
public class PageDTO {
    private Integer count;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public PageDTO() {

    }
    public PageDTO(Integer count) {
        this.count = count;
    }

    public static PageDTO setPage(Integer count){
        return new PageDTO(count);
    }
}
