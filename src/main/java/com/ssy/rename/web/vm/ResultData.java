package com.ssy.rename.web.vm;


import com.ssy.rename.web.vm.dto.PageDTO;
import com.ssy.rename.web.vm.enums.ResultStatusEnum;
import org.springframework.http.HttpStatus;

/**
 * create by huangziqideveloper@gmail.com
 */
public class ResultData<T> {
    /** 返回数据 */
	private T data;
    /** 返回code  */
	private int code = HttpStatus.OK.value();
	/** 返回错误信息 */
	private String message = HttpStatus.OK.getReasonPhrase();

//	private String sort;

//	private String order;

//	private Integer page;

//	private Integer perPage;

	private PageDTO page;


	public ResultData() {
	}

	public ResultData(T data) {
		this(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), data);
	}

	public ResultData(int code, String message) {
		this(code, message, null);
	}
	public ResultData(int code, T data) {
		this(code, "",data);
	}

	public ResultData(int cdoe, String messagae, T data) {
		this.code = cdoe;
		this.message = messagae;
		this.data = data;
	}

	public ResultData(T data, PageDTO page) {
		this(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), data, page);
	}

	public ResultData(int code, String messagae, T data, PageDTO page) {
		this.code = code;
		this.message = messagae;
		this.data = data;
		this.page = page;
	}
	public ResultData(ResultStatusEnum statusEnum){
		this.code = statusEnum.getCode();
		this.message = statusEnum.getMessage();
	}

	public static <T> ResultData<T> ok() {
		return new ResultData<T>();
	}


	public static <T> ResultData<T> ok(T data) {
		return new ResultData<T>(data);
	}


	public static <T> ResultData<T> layOk(T data) {
		return new ResultData<T>(0, data);
	}
	public static <T> ResultData<T> status(ResultStatusEnum statusEnum) {

		return new ResultData<T>(statusEnum);
	}
	public static <T> ResultData<T> error(int code, String message) {
		return new ResultData<T>(code, message);
	}

	public static <T> ResultData<T> appError() {
		return new ResultData<T>(20007, "系统异常，请稍后再试");
	}

	public static <T> ResultData<T> requiredParamIsNull() {
		return new ResultData<T>(20021, "必要参数为空");
	}

	public static <T> ResultData<T> ok(T data, PageDTO page) {
		return new ResultData<T>(data, page);
	}

	public static <T> ResultData<T> tokenNotFoundError(){
		return new ResultData<T>(20001,"未登录");
	}
	public static <T> ResultData<T> notDownloadPermission(){
		return new ResultData<T>(20006,"没有下载权限");
	}

	public static <T> ResultData<T> fileNotFoundError(){
		return new ResultData<T>(20003,"当前文件不存在");
	}
    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public ResultData setMessage(String message) {
		this.message = message;
		return this;
	}

	public PageDTO getPage() {
		return page;
	}

	public void setPage(PageDTO page) {
		this.page = page;
	}

}
