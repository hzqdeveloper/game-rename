package com.ssy.rename.web.rest;

import com.ssy.rename.app.service.ReNameService;
import com.ssy.rename.web.vm.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/game/name")
@RestController
@CrossOrigin
public class ReNameController {

    @Autowired
    private ReNameService reNameService;

    @GetMapping("/rename")
    public ResultData getReName(String name,String gameCd){
        return reNameService.getReName(name,gameCd);
    }

    @GetMapping("/get/rankinglist")
    public ResultData getRankingList(String name){
        return reNameService.getRankingList(name);
    }

}
