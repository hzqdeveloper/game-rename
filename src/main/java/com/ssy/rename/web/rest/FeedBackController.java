package com.ssy.rename.web.rest;

import com.ssy.rename.app.service.FeedBackService;
import com.ssy.rename.app.service.ReNameService;
import com.ssy.rename.commons.entity.FeedBack;
import com.ssy.rename.web.vm.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/feedback")
@RestController
@CrossOrigin
public class FeedBackController {

    @Autowired
    private FeedBackService feedBackService;

    @PostMapping("/createFeedBack")
    public ResultData createFeedBack(@RequestBody FeedBack feedBack){
        return feedBackService.createFeedBack(feedBack);
    }

}
