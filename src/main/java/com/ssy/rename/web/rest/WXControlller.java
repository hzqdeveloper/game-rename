package com.ssy.rename.web.rest;

import com.alibaba.fastjson.JSONObject;
import com.ssy.rename.app.service.WXService;
import com.ssy.rename.commons.util.WeiXinResponseWrapper;
import com.ssy.rename.web.vm.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/wx")
@CrossOrigin
public class WXControlller {

    @Autowired
    private WXService wxService;

    @GetMapping("/getAccessToken")
    public ResultData getAccessToken(){
        return ResultData.ok(wxService.getAccessToken());
    }

    @GetMapping("/checkText")
    public JSONObject checkText(String text){
        return wxService.checkText(text).getData();
    }
}
