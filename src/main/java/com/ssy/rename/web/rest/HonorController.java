package com.ssy.rename.web.rest;

import com.ssy.rename.app.service.HonorService;
import com.ssy.rename.commons.entity.wzry.HeroBean;
import com.ssy.rename.commons.entity.wzry.RankListBean;
import com.ssy.rename.commons.entity.wzry.WZRYProvince;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/api/game/wzry/honor")
@RestController
@CrossOrigin
public class HonorController {
    @Autowired
    private HonorService honorService;

//
//    @GetMapping("/ranklist")
//    public List<RankListBean> getRankList(){
//        return honorService.getRankList();
//    }

    @GetMapping("/heroList")
    public List<HeroBean> getDistricts(){
        return honorService.getHeroList();
    }


    @GetMapping("/meger")
    public String megerHonorList(){
         honorService.megerHonorList();
         return "ok";
    }


}
