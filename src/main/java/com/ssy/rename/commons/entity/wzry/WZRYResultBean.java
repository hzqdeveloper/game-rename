package com.ssy.rename.commons.entity.wzry;

public class WZRYResultBean {

    private Integer result;
    private Integer returnCode;
    private String returnMsg;

    private WZRYDataBean data;

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public Integer getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(Integer returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public WZRYDataBean getData() {
        return data;
    }

    public void setData(WZRYDataBean data) {
        this.data = data;
    }
}
