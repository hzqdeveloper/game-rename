package com.ssy.rename.commons.entity;

import java.io.Serializable;

public class FeedBack implements Serializable {
    private Integer id;
    private String text;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public FeedBack() {

    }

    public FeedBack(Integer id, String text) {
        this.id = id;
        this.text = text;
    }
}
