package com.ssy.rename.commons.entity.wzry;

import java.util.List;

public class DistrictsRankListBean {

    private Integer adcode;

    private List<RankListBean> rankListBeans;

    public Integer getAdcode() {
        return adcode;
    }

    public void setAdcode(Integer adcode) {
        this.adcode = adcode;
    }

    public List<RankListBean> getRankListBeans() {
        return rankListBeans;
    }

    public void setRankListBeans(List<RankListBean> rankListBeans) {
        this.rankListBeans = rankListBeans;
    }
}
