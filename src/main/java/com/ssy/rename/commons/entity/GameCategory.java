package com.ssy.rename.commons.entity;

import java.io.Serializable;

public class GameCategory implements Serializable {


    private Integer id;
    private String gameName;
    private String gameCd;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getGameCd() {
        return gameCd;
    }

    public void setGameCd(String gameCd) {
        this.gameCd = gameCd;
    }
}
