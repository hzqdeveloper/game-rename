package com.ssy.rename.commons.entity.wzry;

import java.util.List;

public class WZRYProvince {

    private String adcode;
    private String shortName;
    private String fullName;

    private List<WZRYCity> list;

    public List<WZRYCity> getList() {
        return list;
    }

    public void setList(List<WZRYCity> list) {
        this.list = list;
    }

    public String getAdcode() {
        return adcode;
    }

    public void setAdcode(String adcode) {
        this.adcode = adcode;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
