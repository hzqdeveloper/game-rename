package com.ssy.rename.commons.entity;

import java.io.Serializable;

public class ReNameHistory implements Serializable {
    private Long id;
    private String userGameName;
    private String gameCd;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserGameName() {
        return userGameName;
    }

    public void setUserGameName(String userGameName) {
        this.userGameName = userGameName;
    }

    public String getGameCd() {
        return gameCd;
    }

    public void setGameCd(String gameCd) {
        this.gameCd = gameCd;
    }

    @Override
    public String toString() {
        return "ReNameHistory{" +
                "id=" + id +
                ", userGameName='" + userGameName + '\'' +
                ", gameCd='" + gameCd + '\'' +
                '}';
    }

    public ReNameHistory() {
    }

    public ReNameHistory(String userGameName, String gameCd) {
        this.userGameName = userGameName;
        this.gameCd = gameCd;
    }
}
