package com.ssy.rename.commons.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ByteUtil {

    @Autowired
    private UnicodeUtil unicodeUtil;

    public int getBytes(String name){

        int bytes = 0;
        for (int i = 0; i < name.length(); i++) {
            if(unicodeUtil.isChinese(name.charAt(i))){
                bytes += 2;
            }else {
                bytes += 1;
            }
        }
        return bytes;
    }
}
