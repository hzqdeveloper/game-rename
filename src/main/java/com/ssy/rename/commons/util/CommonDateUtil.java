package com.ssy.rename.commons.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * create by huangziqideveloper@gmail.com
 */
public class CommonDateUtil {

	public static final String DATE_FORMAT_DETAIL = "yyyy-MM-dd HH:mm:ss";

	public static final String DATE_SHORT_DETAIL = "yyyy-MM-dd";

	public static final String DATE_SIMPLESHORT_DETAIL = "yyyyMMdd";

	private static final Log logger = LogFactory.getLog(CommonDateUtil.class);

	/**
	 * 指定类型的字符串转换为时间<br>
	 * <br>
	 * @param dateStr
	 * @param format
	 * @return
	 * @ahthor zhaofengling
	 * @since 2015-4-23 
	 */
	public static Date parseToDate(String dateStr, String format) {
		Date result = null;
		if(null == format || "".equals(format)) {
			format = DATE_FORMAT_DETAIL;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			result = sdf.parse(dateStr);
		} catch(Exception e) {
			logger.error("日期转换错误", e);
		}
		return result;
	}

	/**
	 *  将时间转换为指定类型的字符串.<br>
	 * <br>
	 * @param date
	 * @param format
	 * @return
	 * @ahthor zhaofengling
	 * @since 2015-4-23 
	 */
	public static String dateToString(Date date, String format) {
		String result = "";

		if(null == format || "".equals(format)) {
			format = DATE_FORMAT_DETAIL;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(format);
		if(date != null) {
			result = sdf.format(date);
		}

		return result;
	}

	/**
	 * 计算两个日期相差的天数<br>
	 * <br>
	 * @param smdate
	 * @param bdate
	 * @return
	 * @ahthor zhaofengling
	 * @since 2015-4-25 
	 */
	public static int daysBetween(Date smdate, Date bdate) {
		int result = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			smdate = sdf.parse(sdf.format(smdate));
			bdate = sdf.parse(sdf.format(bdate));
		} catch(ParseException e) {
			logger.error("日期转换出错");
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(smdate);
		long time1 = cal.getTimeInMillis();
		cal.setTime(bdate);
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);

		result = Integer.parseInt(String.valueOf(between_days));

		return result;
	}

	/**
	 * 任意字符串日期格式化为标准型
	 * TODO 出错则返回0001-01-01 00:00:00.<br>
	 * <br>
	 * @param dateStr
	 * @return
	 * @ahthor Shili Chen.
	 * @since 2015-5-25
	 */
	public static Date parseToDate(String dateStr) {
		dateStr = CommonDateUtil.FormatDate(dateStr);
		return CommonDateUtil.parseToDate(dateStr, DATE_FORMAT_DETAIL);
	}

	/**
	 * 正则日期格式化字符串为标准型
	 * TODO 出错则返回0001-01-01 00:00:00.<br>
	 * <br>
	 * @param dateStr
	 * @return
	 * @ahthor Shili Chen.
	 * @since 2015-5-25
	 * 注：此文件有改动，详情请看日志
	 */
	@SuppressWarnings({"finally", "unused"})
	public static String FormatDate(String dateStr) {

		HashMap<String, String> dateRegFormat = new HashMap<String, String>();
		dateRegFormat.put("^\\d{4}\\D+\\d{1,2}\\D+\\d{1,2}\\D+\\d{1,2}\\D+\\d{1,2}\\D+\\d{1,2}\\D*$", "yyyy-MM-dd-HH-mm-ss");//2014年3月12日 13时5分34秒，2014-03-12 12:05:34，2014/3/12 12:5:34
		dateRegFormat.put("^\\d{4}\\D+\\d{2}\\D+\\d{2}\\D+\\d{2}\\D+\\d{2}$", "yyyy-MM-dd-HH-mm");//2014-03-12 12:05
		dateRegFormat.put("^\\d{4}\\D+\\d{2}\\D+\\d{2}\\D+\\d{2}$", "yyyy-MM-dd-HH");//2014-03-12 12
		dateRegFormat.put("^\\d{4}\\D+\\d{2}\\D+\\d{2}$", "yyyy-MM-dd");//2014-03-12
		dateRegFormat.put("^\\d{4}\\D+\\d{2}$", "yyyy-MM");//2014-03
		dateRegFormat.put("^\\d{4}$", "yyyy");//2014
		dateRegFormat.put("^\\d{14}$", "yyyyMMddHHmmss");//20140312120534
		dateRegFormat.put("^\\d{12}$", "yyyyMMddHHmm");//201403121205
		dateRegFormat.put("^\\d{10}$", "yyyyMMddHH");//2014031212
		dateRegFormat.put("^\\d{8}$", "yyyyMMdd");//20140312
		dateRegFormat.put("^\\d{6}$", "yyyyMM");//201403
		dateRegFormat.put("^\\d{2}\\s*:\\s*\\d{2}\\s*:\\s*\\d{2}$", "yyyy-MM-dd-HH-mm-ss");//13:05:34 拼接当前日期
		dateRegFormat.put("^\\d{2}\\s*:\\s*\\d{2}$", "yyyy-MM-dd-HH-mm");//13:05 拼接当前日期
		dateRegFormat.put("^\\d{2}\\D+\\d{1,2}\\D+\\d{1,2}$", "yy-MM-dd");//14.10.18(年.月.日)
		dateRegFormat.put("^\\d{1,2}\\D+\\d{1,2}$", "yyyy-dd-MM");//30.12(日.月) 拼接当前年份
		dateRegFormat.put("^\\d{1,2}\\D+\\d{1,2}\\D+\\d{4}$", "dd-MM-yyyy");//12.21.2013(日.月.年)

		String curDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat formatter2;
		String dateReplace;
		String strSuccess = "";
		try {
			for(String key : dateRegFormat.keySet()) {
				if(Pattern.compile(key).matcher(dateStr).matches()) {
					formatter2 = new SimpleDateFormat(dateRegFormat.get(key));
					if(key.equals("^\\d{2}\\s*:\\s*\\d{2}\\s*:\\s*\\d{2}$") || key.equals("^\\d{2}\\s*:\\s*\\d{2}$")) {//13:05:34 或 13:05 拼接当前日期
						dateStr = curDate + "-" + dateStr;
					} else if(key.equals("^\\d{1,2}\\D+\\d{1,2}$")) {//21.1 (日.月) 拼接当前年份
						dateStr = curDate.substring(0, 4) + "-" + dateStr;
					}
					dateReplace = dateStr.replaceAll("\\D+", "-");
					// System.out.println(dateRegExpArr[i]);
					strSuccess = formatter1.format(formatter2.parse(dateReplace));
					break;
				}
			}
		} catch(Exception e) {
			strSuccess = "0001-01-01 00:00:00";//设置为最小日期
			logger.error("-----------------日期格式无效:" + dateStr);
		}
		return strSuccess;
	}
}
