package com.ssy.rename.commons.util;

import com.alibaba.fastjson.JSONObject;


/**
 * WeiXinResponseWrapper
 *
 * @author
 * @date 2019/3/13
 */

public class WeiXinResponseWrapper {


    private String errCode;
    private String errMsg;
    private JSONObject data;

    public static WeiXinResponseWrapper wrap(JSONObject jo) {
        return new WeiXinResponseWrapper(jo.getString("errcode"), jo.getString("errmsg"), jo);
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public JSONObject getData() {
        return data;
    }

    public WeiXinResponseWrapper() {
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    public WeiXinResponseWrapper(String errCode, String errMsg, JSONObject data) {
        this.errCode = errCode;
        this.errMsg = errMsg;
        this.data = data;
    }

    public boolean isRequestSucceed() {
        return errCode==null||"0".equals(errCode);
    }

    public String get(String key) {
        return data.getString(key);
    }
}
