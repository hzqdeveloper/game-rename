package com.ssy.rename.commons.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;


/**
 * List工具类
 * create by huangziqideveloper@gmail.com
 */
public class ListUtil<T> {

	private static final Log logger = LogFactory.getLog(ListUtil.class);

	/**
	 * 
	 * TODO List 排序方法.<br>
	 * <br>
	 * @param list 需要排序的list
	 * @param orderKey 根据排序的get方法名称
	 * @param sort ASC/DESC
	 * @ahthor Shili Chen.
	 * @since 2015-5-6
	 */
	public void sortList(List<T> list, final String orderKey, final String sort, final SortTypeEnum sortType) {
		logger.debug("list：" + orderKey + "字段排序：" + sort);
		// 用内部类实现排序  
		Collections.sort(list, new Comparator<T>() {

			public int compare(T a, T b) {
				int ret = 0;
				try {
					ret = singleCompareT(a, b, orderKey, sort, sortType.toValue());
				} catch(NoSuchMethodException ne) {
					logger.error(ne);
				} catch(IllegalArgumentException e) {
					logger.error(e);
				} catch(IllegalAccessException e) {
					logger.error(e);
				} catch(InvocationTargetException e) {
					logger.error(e);
				}
				return ret;
			}
		});
	}

	/**
	 * 
	 * TODO List 复杂2次排序.<br>
	 * <br>
	 * @param list 需要排序的list
	 * @param orderKey 根据排序的get方法名称
	 * @param sort ASC/DESC
	 * @param secondKey 如果相等，则根据排序的get方法名称
	 * @param secondSort 如果相等， ASC/DESC
	 * @ahthor Shili Chen.
	 * @since 2015-5-23
	 */
	public void sortList(List<T> list, final String orderKey, final String sort, final SortTypeEnum firstType,
			final String secondKey, final String secondSort, final SortTypeEnum secondType) {
		logger.debug("list：" + orderKey + "字段排序：" + sort + " second:" + secondKey + "字段排序：" + secondSort);
		// 用内部类实现排序  
		Collections.sort(list, new Comparator<T>() {

			public int compare(T a, T b) {
				int ret = 0;
				try {
					ret = singleCompareT(a, b, orderKey, sort, firstType.toValue());
					if(ret == 0) {
						ret = singleCompareT(a, b, secondKey, secondSort, secondType.toValue());
					}
				} catch(NoSuchMethodException ne) {
					logger.error(ne);
				} catch(IllegalArgumentException e) {
					logger.error(e);
				} catch(IllegalAccessException e) {
					logger.error(e);
				} catch(InvocationTargetException e) {
					logger.error(e);
				}
				return ret;
			}
		});
	}

	/**
	 * 
	 * TODO 单次比较.<br>
	 * <br>
	 * @param a 对象a
	 * @param b 对象b
	 * @param key 比较的键
	 * @param sort ASC/DESC
	 * @param type 字段类型  1.string 2.int 3.float 4.date
	 * @return
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @ahthor Shili Chen.
	 * @since 2015-5-23
	 */
	private int singleCompareT(T a, T b, final String key, final String sort, final int type) throws NoSuchMethodException,
			SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Method m1 = a.getClass().getMethod(key, null);
		Method m2 = b.getClass().getMethod(key, null);
		Object o1 = m1.invoke(((T)a), null);
		Object o2 = m2.invoke(((T)b), null);
		int compareResult = 0;
		switch(type) {
			case 1://字符串比较
				compareResult = o1.toString().compareTo(o2.toString());
				break;
			case 2://整型比较
				int i1 = Integer.MIN_VALUE;
				int i2 = Integer.MIN_VALUE;
				try {
					i1 = Integer.parseInt(o1.toString());
				} catch(Exception e) {
					logger.error(e);
				}
				try {
					i2 = Integer.parseInt(o2.toString());
				} catch(Exception e) {
					logger.error(e);
				}
				if(i1 > i2) {
					compareResult = 1;
				} else if(i1 < i2) {
					compareResult = -1;
				} else {
					compareResult = 0;
				}
				break;
			case 3://浮点型比较
				float f1 = Float.MIN_VALUE;
				float f2 = Float.MIN_VALUE;
				try {
					f1 = Float.parseFloat(o1.toString());
				} catch(Exception e) {
					logger.error(e);
				}
				try {
					f2 = Float.parseFloat(o2.toString());
				} catch(Exception e) {
					logger.error(e);
				}
				if(f1 > f2) {
					compareResult = 1;
				} else if(f1 < f2) {
					compareResult = -1;
				} else {
					compareResult = 0;
				}
				break;
			case 4:
				Date d1 = CommonDateUtil.parseToDate(o1.toString());
				Date d2 = CommonDateUtil.parseToDate(o2.toString());
				compareResult = d1.compareTo(d2);
				break;
			default:
				compareResult = o1.toString().compareTo(o2.toString());
				break;
		}
		if(sort != null && "desc".equals(sort.toLowerCase())) {
			return -compareResult;
		} else {
			// 正序排序  
			return compareResult;
		}
	}

	/**
	 * TODO List 查询方法.<br>
	 * <br>
	 * @param list list
	 * @param whereKey get方法
	 * @param whereValue 值
	 * @param 对比符号 like或其他
	 * @return
	 * @ahthor Shili Chen.
	 * @since 2015-5-23
	 */
	public List<T> whereList(List<T> list, String whereKey, String whereValue, String ratio) {
		List<T> sublist = new ArrayList();
		if(list != null) {
			for(int i = 0; i < list.size(); i++) {
				try {
					Method m = list.get(i).getClass().getMethod(whereKey, null);
					String value = m.invoke((list.get(i)), null) == null ? null : m.invoke((list.get(i)), null).toString();
					boolean isSet = false;
					if(value != null) {
						switch(ratio) {
							case "like":
								if(value.indexOf(whereValue) >= 0 || whereValue.indexOf(value) >= 0) {
									isSet = true;
								}
								break;
							default:
								if(value.equals(whereValue)) {
									isSet = true;
								}
								break;
						}
					}
					if(isSet) {
						sublist.add(list.get(i));
					}
				} catch(Exception e) {
					logger.error(e);
					continue;
				}
			}
		}
		return sublist;
	}

	/**
	 * 排序枚举
	 * The class SortTypeEnum<br>
	 * <br>
	 * TODO 설명을 상세히 입력하세요.<br>
	 * <br>
	 * <br>
	 * Copyright (c) 2015 CJ OliveNetworks<br>
	 * All rights reserved.<br>
	 * <br>
	 * This software is the proprietary information of CJ OliveNetworks<br>
	 * <br>
	 * @author Shili Chen.
	 * @version 2.0
	 * @since 2015-5-25
	 *
	 */
	public enum SortTypeEnum {

		STRING(1), INT(2), FLOAT(3), DATE(4);

		private int type;

		private SortTypeEnum(int type) {
			this.type = type;
		}

		public int toValue() {
			return type;
		}
	}

}
