package com.ssy.rename.commons.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * create by huangziqideveloper@gmail.com
 */
@Component
public class SpringApplicationUtil implements ApplicationContextAware {

    private static ApplicationContext applicationContext;


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    private void checkContext(){
        if (applicationContext == null) {
            throw new IllegalStateException(
                    "未注入SpringContext");
        }
    }

    public <T> T getBean(String name){
        checkContext();
        return (T)applicationContext.getBean(name);
    }

    public <T> T getBean(Class<T> clazz){
        checkContext();
        return (T)applicationContext.getBeansOfType(clazz);
    }

}
