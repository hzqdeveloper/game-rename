package com.ssy.rename.commons.util;

import org.springframework.stereotype.Component;

@Component
public class RandomUtils {

    public int getRandom(){
        return 1 + (int)(Math.random()*5);
    }

    public boolean useUnicode(){
        int a =  1+(int)(Math.random()*2);
        return a>1;
    }
}
