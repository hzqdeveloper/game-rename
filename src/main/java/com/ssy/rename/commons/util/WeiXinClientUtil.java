package com.ssy.rename.commons.util;

import com.alibaba.fastjson.JSON;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;

import java.io.IOException;


public class WeiXinClientUtil {

    private WeiXinClientUtil() {
    }

    public static WeiXinResponseWrapper sendGetRequest(String path, NameValuePair[] pairs) throws IOException {
        HttpClient client = new HttpClient();
        GetMethod method = new GetMethod();
        method.setPath(path);
        method.setQueryString(pairs);
        client.executeMethod(method);
        byte[] bytes = method.getResponseBody();
        return WeiXinResponseWrapper.wrap(JSON.parseObject(new String(bytes)));
    }

    public static WeiXinResponseWrapper sendPostRequest(String path, NameValuePair[] pairs, String body) throws IOException {
        HttpClient client = new HttpClient();
        PostMethod method = new PostMethod();
        method.setPath(path);
        method.setQueryString(pairs);
        RequestEntity entity = new StringRequestEntity(body, "application/json", "utf-8");
        method.setRequestEntity(entity);
        client.executeMethod(method);
        byte[] bytes = method.getResponseBody();
        return WeiXinResponseWrapper.wrap(JSON.parseObject(new String(bytes)));
    }

}
