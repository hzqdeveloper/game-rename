package com.ssy.rename.commons.util;

import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * create by huangziqideveloper@gmail.com
 */
public class DateUtils {
    public static String formatDate(Date d, String patten) {
        SimpleDateFormat format = new SimpleDateFormat(patten);
        return format.format(d);
    }

    public static String getNow() {
        return formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
    }

    public static String getNowDate() {
        return formatDate(new Date(), "yyyy-MM-dd");
    }

    public static String getNowDateN() {
        return formatDate(new Date(), "yyyyMMdd");
    }

    public static String getNowTime() {
        return formatDate(new Date(), "HHmmss");
    }

    public static String getNowYear() {
        return formatDate(new Date(), "yyyy");
    }

    public static String getNowMonth() {
        return formatDate(new Date(), "MM");
    }

    public static String getNowDay() {
        return formatDate(new Date(), "dd");
    }

    /**
     * 获取时间的-6小时制
     *
     * @param date
     * @return
     */
    public static String getEsbDate(String date) {
        String fmt = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(fmt);
        try {
            Date d = simpleDateFormat.parse(date);
            return get6(d);
        } catch (ParseException e) {
            return date.substring(0, 10).replaceAll("-", "");
        }
    }

    /**
     * 获取时间的-6小时制
     *
     * @return
     */
    public static String get6(Date d) {
        String fmt = "yyyyMMdd";
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.add(Calendar.HOUR, -6);
        String defMon = new SimpleDateFormat(fmt).format(c.getTime());
        return defMon;
    }

    /**
     * redis缓存机制   23:59:59
     *
     * @return
     */
    public static long redisExpireTime() {
        DateTime nowDateTime = DateTime.now();
        DateTime endDateTime = nowDateTime.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);
        return endDateTime.minus(nowDateTime.getMillis()).getMillis() / 1000L;
    }

    /**
     * 缓存击穿5分钟策略
     *
     * @return
     */
    public static long redisExpireTimeFiveSecond() {
        return 60 * 5;
    }

    public static String get6() {
        return get6(new Date());
    }

    public static String getDate(Date d, String patten) {
        return formatDate(d, patten);
    }

    public static String getNow(String patten) {
        return formatDate(new Date(), patten);
    }

    public static Date getDate(String date, String fmt) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(fmt);
        try {
            return simpleDateFormat.parse(date);
        } catch (ParseException e) {
            return new Date();
        }
    }

    public static String getAddTime(int field, int amount, String partten) {
        Calendar c = Calendar.getInstance();
        c.add(field, amount);
        String def = new SimpleDateFormat(partten).format(c.getTime());
        return def;
    }

    public static String getAddTime(int field, int amount, String partten, String date) {
        SimpleDateFormat sd = new SimpleDateFormat(partten);
        Calendar c = Calendar.getInstance();
        try {
            Date d = sd.parse(date);
            c.setTime(d);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            c.setTime(new Date());
        }
        c.add(field, amount);
        String defMon = new SimpleDateFormat(partten).format(c.getTime());
        return defMon;
    }

    public static String getNowDateTime() {
        return formatDate(new Date(), "yyyyMMddHHmmss");
    }
}
