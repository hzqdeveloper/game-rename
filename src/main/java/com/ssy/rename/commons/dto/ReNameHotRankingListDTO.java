package com.ssy.rename.commons.dto;


import java.io.Serializable;
import java.util.Date;

public class ReNameHotRankingListDTO implements Serializable {

  private String userGameName;
  private Integer nameCount;

  public String getUserGameName() {
    return userGameName;
  }

  public void setUserGameName(String userGameName) {
    this.userGameName = userGameName;
  }

  public Integer getNameCount() {
    return nameCount;
  }

  public void setNameCount(Integer nameCount) {
    this.nameCount = nameCount;
  }
}
