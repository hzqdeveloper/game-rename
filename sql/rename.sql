/*
Navicat MySQL Data Transfer

Source Server         : 43.128.27.123
Source Server Version : 50650
Source Host           : 43.128.27.123:3306
Source Database       : rename

Target Server Type    : MYSQL
Target Server Version : 50650
File Encoding         : 65001

Date: 2021-11-09 23:15:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for feedback
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `text` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of feedback
-- ----------------------------

-- ----------------------------
-- Table structure for game_category
-- ----------------------------
DROP TABLE IF EXISTS `game_category`;
CREATE TABLE `game_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_name` varchar(50) DEFAULT NULL,
  `game_cd` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of game_category
-- ----------------------------
INSERT INTO `game_category` VALUES ('1', '王者荣耀', 'wzry');
INSERT INTO `game_category` VALUES ('2', '英雄联盟手游', 'lolm');
INSERT INTO `game_category` VALUES ('3', '穿越火线枪战王者', 'cfm');
INSERT INTO `game_category` VALUES ('4', '和平精英', 'hpjy');

-- ----------------------------
-- Table structure for rename_history
-- ----------------------------
DROP TABLE IF EXISTS `rename_history`;
CREATE TABLE `rename_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_game_name` varchar(50) DEFAULT NULL,
  `game_cd` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rename_history
-- ----------------------------
INSERT INTO `rename_history` VALUES ('8', '快乐小菜鸡', 'wzry');
INSERT INTO `rename_history` VALUES ('9', '戏命师', 'lolm');
INSERT INTO `rename_history` VALUES ('10', '一杯美式', 'wzry');
INSERT INTO `rename_history` VALUES ('11', 'miss.you', 'wzry');
INSERT INTO `rename_history` VALUES ('12', '稻香', 'lolm');
INSERT INTO `rename_history` VALUES ('13', '习研会会员群', 'lolm');
INSERT INTO `rename_history` VALUES ('14', '周星驰', 'wzry');
INSERT INTO `rename_history` VALUES ('15', '不敢奢望了', 'wzry');
INSERT INTO `rename_history` VALUES ('16', '幼儿园扛把子', 'lolm');
INSERT INTO `rename_history` VALUES ('17', '少年', 'lolm');
